package org.it.manejadores;

import org.it.beans.Reportes;
import org.it.db.Conexion;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ManejadorReportes {

	public static final ManejadorReportes INSTANCIA = new ManejadorReportes();
	private ArrayList<Reportes> listaR;
	
	public ArrayList<Reportes> getLista(){
		actualizarLista();
		return this.listaR;
	}
	
	public void actualizarLista(){
		ResultSet r=Conexion.getInstancia().obtenerConsulta("SELECT idReporte,fechaReporte,descripcion,DPI,idArea,idEstado FROM Reportes ORDER BY idReporte DESC");
		listaR= new ArrayList<Reportes>();
		try{
			while(r.next()){
				Reportes rep=new Reportes(r.getInt("idReporte"),r.getInt("idArea"),r.getInt("idEstado"),
						r.getString("descripcion"),r.getLong("DPI"),r.getDate("fechaReporte"));
				rep.setArea(ManejadorArea.INSTANCIA.getArea(r.getInt("idArea")));
				rep.setEstado(ManejadorEstado.INSTANCIA.getEstado(r.getInt("idEstado")));
				if(r.getInt("idEstado")==1){
					rep.setClase("info");
				}else if(r.getInt("idEstado")==2){
					rep.setClase("active");
				}else if(r.getInt("idEstado")==3){
					rep.setClase("warning");
				}else if(r.getInt("idEstado")==4){
					rep.setClase("success");
				}else{
					rep.setClase("danger");
				}
				this.listaR.add(rep);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	public void agregarReporte(Reportes r){
		Date date = new Date();
		DateFormat dateHourFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss ");
		Conexion.getInstancia().ejecutarConsulta("INSERT INTO Reportes(fechaReporte,descripcion,DPI,idArea,idEstado) VALUES('"+dateHourFormat.format(date)+"','"+r.getDescripcion()+"',"+r.getDPI()+","+r.getIdArea()+","+r.getIdEstado()+") ");
	}
	
	public void editarReporte(Reportes r){
		Conexion.getInstancia().ejecutarConsulta("UPDATE Reportes SET descripcion='"+r.getDescripcion()+"',idEstado="+r.getIdEstado()+" WHERE idReporte="+r.getIdReporte());
	}
	
	public ArrayList<Reportes> getListaUsuario(){
		ArrayList<Reportes> listaRep=new ArrayList<Reportes>();
		actualizarLista();
		for(Reportes rep:this.listaR){
			if(rep.getDPI()==ManejadorUsuario.INSTANCIA.getUsuarioAutenticad().getDPI())
				listaRep.add(rep);
		}
		return listaRep;
		
	}
	
	public Reportes getReporte(int id){
		Reportes r=null;
		actualizarLista();
		for(Reportes rep:this.listaR){
			if(rep.getIdReporte()==id)
				r=rep;
		}
		return r;
		
	}
	
	public ArrayList<Reportes> getListaPorRevisar(){
		ArrayList<Reportes> listaReportes = new ArrayList<Reportes>();
		for(Reportes rep:getLista()){
			if(rep.getIdEstado()==1){
				listaReportes.add(rep);
			}
		}
		return listaReportes;
	}
	
}