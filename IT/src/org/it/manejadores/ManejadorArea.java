package org.it.manejadores;

import org.it.beans.Area;
import org.it.db.Conexion;

import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ManejadorArea {

	public static final ManejadorArea INSTANCIA=new ManejadorArea();
	private ArrayList<Area> listaA;
	
	public ArrayList<Area> getLista(){
		actualizarLista();
		return listaA;
	}
	
	public void actualizarLista(){
		ResultSet r=Conexion.getInstancia().obtenerConsulta("SELECT idArea,nombreArea FROM Area ORDER BY idArea ASC");
		listaA = new ArrayList<Area>();
		try{
			while(r.next()){
				this.listaA.add(new Area(r.getInt("idArea"),r.getString("nombreArea")));
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	public void agregarArea(String nombreArea){
		Conexion.getInstancia().ejecutarConsulta("INSERT INTO Area(nombreArea) VALUES('"+nombreArea+"')");
	}
	
	public void editarArea(Area a){
		Conexion.getInstancia().ejecutarConsulta("UPDATE Area SET nombreArea='"+a.getNombreArea()+"' WHERE idArea="+a.getIdArea());
	}
	
	public void eliminarArea(int idA){
		Conexion.getInstancia().ejecutarConsulta("DELETE FROM Area WHERE idArea="+idA);
	}
	
	public String getArea(int id){
		String a=null;
		actualizarLista();
		for(Area area:listaA){
			if(area.getIdArea()==id){
				a=area.getNombreArea();
			}
		}
		return a;
	}
	
}
