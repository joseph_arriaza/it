package org.it.manejadores;

import java.util.ArrayList;
import java.util.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.it.beans.Historial;
import org.it.db.Conexion;

public class ManejadorHistorial {

	public static final ManejadorHistorial INSTANCIA = new ManejadorHistorial();
	private ArrayList<Historial> listaH;
	
	public ArrayList<Historial> getLista(){
		actualizarLista();
		return this.listaH;
	}

	public void actualizarLista(){
		ResultSet r=Conexion.getInstancia().obtenerConsulta("SELECT idHistorial,DPI,idTicket,creacion,finalizacion FROM Historial ORDER BY idHistorial DESC");
		listaH= new ArrayList<Historial>();
		try{
			while(r.next()){
				Historial h=new Historial(r.getInt("idHistorial"),r.getInt("idTicket"),r.getLong("DPI"),r.getDate("creacion"),r.getDate("finalizacion"));
				this.listaH.add(h);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	public void agregarHistorial(Historial h){
		Conexion.getInstancia().ejecutarConsulta("INSERT INTO Historial(creacion,finalizacion,DPI,idTicket) VALUES('"+h.getCreacion()+"','',"+h.getDPI()+","+h.getIdTicket()+")");
	}
	
	public void finalizar(int id,Date finalizacion){
		Conexion.getInstancia().ejecutarConsulta("UPDATE Historial SET finalizacion='"+finalizacion+"' WHERE idHistorial="+id);
	}
}
