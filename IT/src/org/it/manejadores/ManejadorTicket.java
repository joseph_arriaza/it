package org.it.manejadores;

import org.it.beans.Ticket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.it.beans.Usuario_Ticket;
import org.it.db.Conexion;

import java.util.ArrayList;
import java.sql.SQLException;
import java.sql.ResultSet;

public class ManejadorTicket {

	public static final ManejadorTicket INSTANCIA = new ManejadorTicket();
	private ArrayList<Ticket> listaT;
	
	public ArrayList<Ticket> getLista(){
		actualizarLista();
		return this.listaT;
	}
	
	public void actualizarLista(){
		ResultSet r=Conexion.getInstancia().obtenerConsulta("SELECT * FROM Ticket ORDER BY idTicket ASC");
		listaT=new ArrayList<Ticket>();
		try{
			while(r.next()){
				Ticket t=new Ticket(r.getInt("idTicket"),r.getInt("idArea"),r.getInt("idEstado"),r.getString("nombreTicket"),
						r.getString("descripcion"),r.getString("notas"),r.getDate("inicio"));
				if(r.getDate("fin")!=null){
					t.setFin(r.getDate("fin"));
				}
				t.setNombreA(ManejadorArea.INSTANCIA.getArea(r.getInt("idArea")));
				t.setNombreE(ManejadorEstado.INSTANCIA.getEstado(r.getInt("idEstado")));
				if(r.getInt("idEstado")==1){
					t.setClase("info");
				}else if(r.getInt("idEstado")==2){
					t.setClase("active");
				}else if(r.getInt("idEstado")==3){
					t.setClase("warning");
				}else if(r.getInt("idEstado")==4){
					t.setClase("success");
				}else{
					t.setClase("danger");
				}
				this.listaT.add(t);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	public void agregarTicket(Ticket t){
		Date date = new Date();
		DateFormat dateHourFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss ");
		Conexion.getInstancia().ejecutarConsulta("INSERT INTO Ticket(nombreTicket,inicio,fin,descripcion,notas,idEstado,idArea) VALUES('"+t.getNombreTicket()+"','"+dateHourFormat.format(date)+"',null,'"+t.getDescripcion()+"',null,"+t.getIdEstado()+","+t.getIdArea()+")");
	}
	
	public void editarTicket(Ticket t){
		Conexion.getInstancia().ejecutarConsulta("UPDATE Ticket SET nombreTicket='"+t.getNombreTicket()+"',descripcion='"+t.getDescripcion()+"' WHERE idTicket="+t.getIdTicket());
	}
	
	public void actualizarTicket(Ticket t){
		if(t.getIdEstado()>2){
			Date date = new Date();
			DateFormat dateHourFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss ");
			Conexion.getInstancia().ejecutarConsulta("UPDATE Ticket SET fin='"+dateHourFormat.format(date)+"',notas='"+t.getNotas()+"',idEstado="+t.getIdEstado()+" WHERE idTicket="+t.getIdTicket());
		}else{
			Conexion.getInstancia().ejecutarConsulta("UPDATE Ticket SET fin=null,notas='"+t.getNotas()+"',idEstado="+t.getIdEstado()+" WHERE idTicket="+t.getIdTicket());
		}
	}
	
	public Ticket getTicket(int id){
		Ticket t=null;
		actualizarLista();
		for(Ticket ti:this.listaT){
			if(ti.getIdTicket()==id){
				t=ti;
			}
		}
		return t;
	}
	
	public ArrayList<Ticket> getListaTecnico(long dpiT){
		ArrayList<Ticket> listaTicket=new ArrayList<Ticket>();
		for(Usuario_Ticket ut:ManejadorUsuario_Ticket.INSTANCIA.getLista()){
			if(ut.getDPI()==dpiT){
				listaTicket.add(getTicket(ut.getIdTicket()));
			}
		}
		return listaTicket;
	}
	
	public ArrayList<Ticket> getListaArea(int idArea){
		ArrayList<Ticket> listaTicket=new ArrayList<Ticket>();
		for(Ticket t:getLista()){
			if(t.getIdArea()==idArea)
				listaTicket.add(t);
		}
		return listaTicket;
	}
	
	public int getLastId(){
		int id=0;
		for(Ticket t:getLista()){
			id=t.getIdTicket();
		}
		return id;
	}
}
