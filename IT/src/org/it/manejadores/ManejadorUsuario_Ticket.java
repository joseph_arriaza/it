package org.it.manejadores;

import org.it.beans.Usuario;
import org.it.beans.Usuario_Ticket;
import org.it.db.Conexion;

import java.util.ArrayList;
import java.sql.SQLException;
import java.sql.ResultSet;

public class ManejadorUsuario_Ticket {

	public static final ManejadorUsuario_Ticket INSTANCIA = new ManejadorUsuario_Ticket();
	private ArrayList<Usuario_Ticket> listaUT;
	
	public ArrayList<Usuario_Ticket> getLista(){
		actualizarLista();
		return this.listaUT;
	}
	
	public void actualizarLista(){
		ResultSet r=Conexion.getInstancia().obtenerConsulta("SELECT DPI,idTicket FROM Usuario_Ticket");
		listaUT=new ArrayList<Usuario_Ticket>();
		try{
			while(r.next()){
				this.listaUT.add(new Usuario_Ticket(r.getLong("DPI"),r.getInt("idTicket")));
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	public void agregarUsuario_Ticket(Usuario_Ticket ut){
		Conexion.getInstancia().ejecutarConsulta("INSERT INTO Usuario_Ticket(DPI,idTicket) VALUES("+ut.getDPI()+","+ut.getIdTicket()+")");
	}
	
	public void editarUsuario_Ticket(Usuario_Ticket ut){
		Conexion.getInstancia().ejecutarConsulta("UPDATE Usuario_Ticket SET DPI="+ut.getDPI()+",idTicket="+ut.getIdTicket()+" WHERE idTicket="+ut.getIdTicket()+" AND DPI="+ut.getDPI());
	}
	
	public ArrayList<Usuario> getTicket(long id){
		ArrayList<Usuario> lista=new ArrayList<Usuario>(); 
		actualizarLista();
		for(Usuario_Ticket ut:this.listaUT){
			if(ut.getIdTicket()==id){
				lista.add(ManejadorUsuario.INSTANCIA.getUser(ut.getDPI()));
			}
		}
		return lista;
	}
	
}
