package org.it.manejadores;

import org.it.db.Conexion;
import org.it.beans.Rol;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ManejadorRol {

	public static final ManejadorRol INSTANCIA=new ManejadorRol();
	private ArrayList<Rol> listaRol;
	
	public void agregarRol(String nombreRol){
		Conexion.getInstancia().ejecutarConsulta("INSERT INTO Rol(nombreRol) VALUES('"+nombreRol+"')");
		actualizarLista();
	}
	
	public void editarRol(Rol rol){
		Conexion.getInstancia().ejecutarConsulta("UPDATE Rol SET nombreRol='"+rol.getNombreRol()+"' WHERE idRol="+rol.getIdRol());
		actualizarLista();
	}
	
	public void actualizarLista(){
		ResultSet resultado = Conexion.getInstancia().obtenerConsulta("SELECT idRol,nombreRol FROM Rol ORDER BY nombreRol ASC");
		listaRol=new ArrayList<Rol>();
		try{
			while(resultado.next()){
				listaRol.add(new Rol(resultado.getInt("idRol"),resultado.getString("nombreRol")));
			}
		}catch(SQLException sqlE){
			sqlE.printStackTrace();
		}
	}
	
	public ArrayList<Rol> getLista(){
		actualizarLista();
		return this.listaRol;
	}
	
	public Rol getRol(int idRol){
		Rol r=null;
		for(Rol rol:getLista()){
			if(rol.getIdRol()==idRol)
				r=rol;
		}
		return r;
	}
	
	public Rol getNombreRol(String nombre){
		Rol r=null;
		for(Rol rol:listaRol){
			if(rol.getNombreRol().equalsIgnoreCase(nombre))
				r=rol;
		}
		return r;
	}
}
