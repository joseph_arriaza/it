package org.it.manejadores;

import org.it.beans.Usuario;
import org.it.db.Conexion;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ManejadorUsuario {

	public static final ManejadorUsuario INSTANCIA = new ManejadorUsuario();
	private ArrayList<Usuario> listaUsuarios;
	private Usuario usuario;
	
	public Usuario getUsuarioAutenticad(){
		return usuario;
	}
	
	public void cerrarSesion(){
		usuario=null;
	}
	
	public void actualizarLista(){
		listaUsuarios=new ArrayList<Usuario>();
		ResultSet r=Conexion.getInstancia().obtenerConsulta("SELECT DPI,email,password,nombre,idRol,estado FROM Usuario");
		try{
			while(r.next()){
				Usuario user=new Usuario(r.getLong("DPI"),r.getString("email"),r.getString("password"),
						r.getString("nombre"),r.getInt("idRol"),r.getInt("estado"));
				user.setRol(ManejadorRol.INSTANCIA.getRol(r.getInt("idRol")).getNombreRol());
				listaUsuarios.add(user);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	public ArrayList<Usuario> getLista(){
		actualizarLista();
		return this.listaUsuarios;
	}

	public boolean iniciarSesion(String email,String password){
		boolean verificar=false;
		actualizarLista();
		for(Usuario user:listaUsuarios){
			if(user.getEmail().equals(email) && user.getPassword().equals(password) && user.getEstado()==1){
				this.usuario=user;
				verificar=true;
			}
		}
		return verificar;
	}
	
	public void agregarUsuario(Usuario user){
		Conexion.getInstancia().ejecutarConsulta("INSERT INTO Usuario(DPI,email,password,nombre,idRol,estado) "
				+ "VALUES("+user.getDPI()+",'"+user.getEmail()+"','"+user.getPassword()+"','"+user.getNombre()+"',"+user.getIdRol()+","+user.getEstado()+")");
	}
	
	public void editarUsuario(Usuario user){
		Conexion.getInstancia().ejecutarConsulta("UPDATE Usuario SET password='"+user.getPassword()+"',nombre='"+user.getNombre()+"' WHERE DPI="+user.getDPI());		
	}
	
	public void cuenta(Usuario user){
		if(user.getEstado()==1){
			Conexion.getInstancia().ejecutarConsulta("UPDATE Usuario SET estado="+0+" WHERE DPI="+user.getDPI());
		}else if(user.getEstado()==0){
			Conexion.getInstancia().ejecutarConsulta("UPDATE Usuario SET estado="+1+" WHERE DPI="+user.getDPI());
		}
	}
	
	public Usuario getUser(long idU){
		Usuario u=null;
		actualizarLista();
		for(Usuario user:listaUsuarios){
			if(user.getDPI()==idU){
				u=user;
			}
		}
		return u;
	}
	
	public  ArrayList<Usuario> getListaTecnicos(){
		 ArrayList<Usuario> listaTecnicos = new  ArrayList<Usuario>();
		 for(Usuario u:getLista()){
			 if(u.getIdRol()==3)
				 listaTecnicos.add(u);
		 }
		 return listaTecnicos;
	}
	
	public boolean verificarCorreo(String email){
		boolean verificar=false;
		for(Usuario u:getLista()){
			if(u.getEmail().equalsIgnoreCase(email))
				verificar=true;
		}
		return verificar;
	}
	
	public boolean verificarDPI(long dpi){
		boolean verificar=false;
		for(Usuario u:getLista()){
			if(u.getDPI()==dpi)
				verificar=true;
		}
		return verificar;
	}
}
