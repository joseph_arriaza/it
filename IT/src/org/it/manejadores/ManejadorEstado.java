package org.it.manejadores;

import java.util.ArrayList;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.it.beans.Estado;
import org.it.db.Conexion;

public class ManejadorEstado {

	public static final ManejadorEstado INSTANCIA = new ManejadorEstado();
	private ArrayList<Estado> listaE;
	
	public ArrayList<Estado> getLista(){
		actualizarLista();
		return this.listaE;
	}
	
	public void actualizarLista(){
		ResultSet r=Conexion.getInstancia().obtenerConsulta("SELECT idEstado,estado FROM Estado ORDER BY idEstado ASC");
		listaE=new ArrayList<Estado>();
		try{
			while(r.next()){
				this.listaE.add(new Estado(r.getInt("idEstado"),r.getString("estado")));
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	public int getIdEstado(String estado){
		int id=0;
		actualizarLista();
		for(Estado e:listaE){
			if(e.getEstado().equalsIgnoreCase(estado))
				id=e.getIdEstado();
		}
		return id;
	}
	
	public String getEstado(int id){
		String es=null;
		actualizarLista();
		for(Estado e:listaE){
			if(e.getIdEstado()==id)
				es=e.getEstado();
		}
		return es;
	}
	
	public ArrayList<Estado> getListaRevisiones(){
		ArrayList<Estado> listaEstados = new ArrayList<Estado>();
		actualizarLista();
		for(Estado e:this.listaE){
			if(e.getIdEstado()>1)
				listaEstados.add(e);
		}
		return listaEstados;
	}
}
