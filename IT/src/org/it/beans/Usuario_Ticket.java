package org.it.beans;

public class Usuario_Ticket {

	private long DPI;
	private int idTicket;
	
	public Usuario_Ticket() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Usuario_Ticket(long dPI, int idTicket) {
		super();
		DPI = dPI;
		this.idTicket = idTicket;
	}

	public long getDPI() {
		return DPI;
	}

	public void setDPI(long dPI) {
		DPI = dPI;
	}

	public int getIdTicket() {
		return idTicket;
	}

	public void setIdTicket(int idTicket) {
		this.idTicket = idTicket;
	}
	
	
	
}
