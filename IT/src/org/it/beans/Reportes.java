package org.it.beans;

import java.util.Date;

public class Reportes {

	private int idReporte,idArea,idEstado;
	private String descripcion;
	private long DPI;
	private Date fechaReporte;
	private String clase,area,estado;
	
	public String getClase() {
		return clase;
	}

	public void setClase(String clase) {
		this.clase = clase;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Reportes(int idReporte, int idArea, int idEstado,
			String descripcion, long dPI, Date fechaReporte) {
		super();
		this.idReporte = idReporte;
		this.idArea = idArea;
		this.idEstado = idEstado;
		this.descripcion = descripcion;
		DPI = dPI;
		this.fechaReporte = fechaReporte;
	}

	public Reportes() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getIdReporte() {
		return idReporte;
	}

	public void setIdReporte(int idReporte) {
		this.idReporte = idReporte;
	}

	public int getIdArea() {
		return idArea;
	}

	public void setIdArea(int idArea) {
		this.idArea = idArea;
	}

	public int getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(int idEstado) {
		this.idEstado = idEstado;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public long getDPI() {
		return DPI;
	}

	public void setDPI(long dPI) {
		DPI = dPI;
	}

	public Date getFechaReporte() {
		return fechaReporte;
	}

	public void setFechaReporte(Date fechaReporte) {
		this.fechaReporte = fechaReporte;
	}
	
	
}
