package org.it.beans;

import java.util.Date;

public class Historial {

	private int idHistorial,idTicket;
	private long DPI;
	private Date creacion,finalizacion;
	
	public Historial() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Historial(int idHistorial, int idTicket, long dPI, Date creacion,
			Date finalizacion) {
		super();
		this.idHistorial = idHistorial;
		this.idTicket = idTicket;
		DPI = dPI;
		this.creacion = creacion;
		this.finalizacion = finalizacion;
	}

	public int getIdHistorial() {
		return idHistorial;
	}

	public void setIdHistorial(int idHistorial) {
		this.idHistorial = idHistorial;
	}

	public int getIdTicket() {
		return idTicket;
	}

	public void setIdTicket(int idTicket) {
		this.idTicket = idTicket;
	}

	public long getDPI() {
		return DPI;
	}

	public void setDPI(long dPI) {
		DPI = dPI;
	}

	public Date getCreacion() {
		return creacion;
	}

	public void setCreacion(Date creacion) {
		this.creacion = creacion;
	}

	public Date getFinalizacion() {
		return finalizacion;
	}

	public void setFinalizacion(Date finalizacion) {
		this.finalizacion = finalizacion;
	}	
}
