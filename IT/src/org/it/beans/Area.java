package org.it.beans;

public class Area {

	private int idArea;
	private String nombreArea;
	
	public Area() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Area(int idArea, String nombreArea) {
		super();
		this.idArea = idArea;
		this.nombreArea = nombreArea;
	}

	public int getIdArea() {
		return idArea;
	}

	public void setIdArea(int idArea) {
		this.idArea = idArea;
	}

	public String getNombreArea() {
		return nombreArea;
	}

	public void setNombreArea(String nombreArea) {
		this.nombreArea = nombreArea;
	}
	
		
}
