package org.it.beans;

public class Usuario {

	private long DPI;
	private String email, password, nombre, rol;
	private int idRol,estado;
	
	
	
	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

	public Usuario(long dPI, String email, String password, String nombre,
			int idRol, int estado) {
		super();
		DPI = dPI;
		this.email = email;
		this.password = password;
		this.nombre = nombre;
		this.idRol = idRol;
		this.estado = estado;
	}

	public Usuario() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getDPI() {
		return DPI;
	}

	public void setDPI(long dPI) {
		DPI = dPI;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getIdRol() {
		return idRol;
	}

	public void setIdRol(int idRol) {
		this.idRol = idRol;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}
	
	
	
}