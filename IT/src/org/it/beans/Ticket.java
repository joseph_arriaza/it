package org.it.beans;

import java.util.Date;

public class Ticket {

	private int idTicket,idArea,idEstado;
	private String nombreTicket, descripcion, notas;
	private Date inicio,fin;
	private String nombreE,nombreA,clase;
	
	
	
	public String getClase() {
		return clase;
	}

	public void setClase(String clase) {
		this.clase = clase;
	}

	public String getNombreE() {
		return nombreE;
	}

	public void setNombreE(String nombreE) {
		this.nombreE = nombreE;
	}

	public String getNombreA() {
		return nombreA;
	}

	public void setNombreA(String nombreA) {
		this.nombreA = nombreA;
	}

	public Ticket(int idTicket, int idArea, int idEstado, String nombreTicket,
			String descripcion, String notas, Date inicio) {
		super();
		this.idTicket = idTicket;
		this.idArea = idArea;
		this.idEstado = idEstado;
		this.nombreTicket = nombreTicket;
		this.descripcion = descripcion;
		this.notas = notas;
		this.inicio = inicio;
	}

	public Ticket() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getIdTicket() {
		return idTicket;
	}

	public void setIdTicket(int idTicket) {
		this.idTicket = idTicket;
	}

	public int getIdArea() {
		return idArea;
	}

	public void setIdArea(int idArea) {
		this.idArea = idArea;
	}

	public int getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(int idEstado) {
		this.idEstado = idEstado;
	}

	public String getNombreTicket() {
		return nombreTicket;
	}

	public void setNombreTicket(String nombreTicket) {
		this.nombreTicket = nombreTicket;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNotas() {
		return notas;
	}

	public void setNotas(String notas) {
		this.notas = notas;
	}

	public Date getInicio() {
		return inicio;
	}

	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

	public Date getFin() {
		return fin;
	}

	public void setFin(Date fin) {
		this.fin = fin;
	} 
	
}
