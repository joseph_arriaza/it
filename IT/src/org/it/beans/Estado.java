package org.it.beans;

public class Estado {

	private int idEstado;
	private String estado;
	
	public Estado() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Estado(int idEstado, String estado) {
		super();
		this.idEstado = idEstado;
		this.estado = estado;
	}

	public int getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(int idEstado) {
		this.idEstado = idEstado;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	
	
}
