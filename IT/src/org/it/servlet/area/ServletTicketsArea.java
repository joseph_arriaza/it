package org.it.servlet.area;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.it.manejadores.ManejadorArea;
import org.it.manejadores.ManejadorTicket;

public class ServletTicketsArea extends HttpServlet{

	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta) throws IOException,ServletException{
		doPost(peticion,respuesta);
	}
		
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta) throws IOException,ServletException{
		RequestDispatcher despachador=peticion.getRequestDispatcher("ticket_area.jsp");
		try{
			peticion.setAttribute("listaTicket_Area",ManejadorTicket.INSTANCIA.getListaArea(Integer.parseInt(peticion.getParameter("idArea"))));
		}catch(Exception e){
			e.printStackTrace();
		}
		despachador.forward(peticion,respuesta);
	}

}
