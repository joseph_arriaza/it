package org.it.servlet.usuario;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.it.manejadores.ManejadorUsuario;

public class ServletDesautenticar extends HttpServlet{

	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta) throws IOException,ServletException{
		doPost(peticion,respuesta);
		
	}
	
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta) throws IOException,ServletException{
		RequestDispatcher despachador=peticion.getRequestDispatcher("index.jsp");
		ManejadorUsuario.INSTANCIA.cerrarSesion();		
		peticion.removeAttribute("estadoLogin");
		despachador.forward(peticion, respuesta);		
	}
	
}
