package org.it.servlet.usuario;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.it.beans.Usuario;
import org.it.manejadores.ManejadorUsuario;

public class ServletEditarUsuario extends HttpServlet{

	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta) throws IOException,ServletException{
		doPost(peticion,respuesta);
	}
	
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta) throws IOException,ServletException{
		RequestDispatcher despachador=peticion.getRequestDispatcher("users.jsp");
		try{
			Usuario u=new Usuario();
			System.out.println(peticion.getParameter("idU"));
			System.out.println(peticion.getParameter("txtNombre"));
			u.setDPI(Long.parseLong(peticion.getParameter("idU")));
			u.setNombre(peticion.getParameter("txtNombre"));
			u.setPassword(peticion.getParameter("txtPass"));
			ManejadorUsuario.INSTANCIA.editarUsuario(u);
			Usuario user=ManejadorUsuario.INSTANCIA.getUsuarioAutenticad();
			user.setNombre(u.getNombre());
			peticion.setAttribute("estadoUsuario", "Editado satisfactoriamente!");
		}catch(Exception e){
			e.printStackTrace();
		}
		despachador.forward(peticion, respuesta);		
	}
	
}