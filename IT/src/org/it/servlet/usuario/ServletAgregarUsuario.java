package org.it.servlet.usuario;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.it.beans.Usuario;
import org.it.manejadores.ManejadorUsuario;

public class ServletAgregarUsuario extends HttpServlet{

	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta) throws IOException,ServletException{
		doPost(peticion,respuesta);
		
	}
	
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta) throws IOException,ServletException{
		RequestDispatcher despachador=peticion.getRequestDispatcher("users.jsp");
		try{
			Usuario u=new Usuario();
			if(peticion.getParameter("txtDPI").length()==14){
				try{
					u.setDPI(Long.parseLong(peticion.getParameter("txtDPI")));
					if(!ManejadorUsuario.INSTANCIA.verificarDPI(u.getDPI())){
						if(!ManejadorUsuario.INSTANCIA.verificarCorreo(peticion.getParameter("txtEmail"))){
							u.setEmail(peticion.getParameter("txtEmail"));
							u.setPassword(peticion.getParameter("txtPass"));
							u.setNombre(peticion.getParameter("txtNombre"));
							u.setIdRol(Integer.parseInt(peticion.getParameter("selectRol")));
							u.setEstado(1);
							ManejadorUsuario.INSTANCIA.agregarUsuario(u);
						}else{
							peticion.setAttribute("estadoAdd", "Ese correo ya esta en uso.");
						}
					}else{
						peticion.setAttribute("estadoAdd", "El DPI ya esta en uso.");
					}
				}catch(NumberFormatException nfe){
					peticion.setAttribute("estadoAdd", "El DPI debe ser numerico.");
				}
			}else{
				peticion.setAttribute("estadoAdd", "El DPI debe tener 14 numeros.");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		despachador.forward(peticion, respuesta);		
	}
	
}