package org.it.servlet.usuario;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.it.manejadores.ManejadorUsuario;

public class ServletAutenticar extends HttpServlet{

	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta) throws IOException,ServletException{
		doPost(peticion,respuesta);
		
	}
	
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta) throws IOException,ServletException{
		RequestDispatcher despachador=peticion.getRequestDispatcher("index.jsp");
		if(ManejadorUsuario.INSTANCIA.iniciarSesion(peticion.getParameter("txtEmail"),peticion.getParameter("txtPass"))){
			peticion.setAttribute("estadoLogin","Inicio");
		}else{
			peticion.setAttribute("estadoLogin","Datos erroneos");
		}
		despachador.forward(peticion, respuesta);		
	}
	
}
