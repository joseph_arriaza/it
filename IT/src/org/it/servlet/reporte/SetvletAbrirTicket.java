package org.it.servlet.reporte;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.it.beans.Reportes;
import org.it.manejadores.ManejadorReportes;
import org.it.manejadores.ManejadorUsuario;
import org.it.manejadores.ManejadorTicket;

public class SetvletAbrirTicket extends HttpServlet{

	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta) throws IOException,ServletException{
		doPost(peticion,respuesta);
	}
		
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta) throws IOException,ServletException{
		RequestDispatcher despachador=null;
		try{
			despachador=peticion.getRequestDispatcher("abrir_ticket.jsp");
			Reportes rep=ManejadorReportes.INSTANCIA.getReporte(Integer.parseInt(peticion.getParameter("idReporte")));
			peticion.setAttribute("reporte",rep);
			peticion.setAttribute("reportador",ManejadorUsuario.INSTANCIA.getUser(rep.getDPI()));
		}catch(Exception e){
			despachador=peticion.getRequestDispatcher("index.jsp");
		}
		despachador.forward(peticion,respuesta);
	}

}
