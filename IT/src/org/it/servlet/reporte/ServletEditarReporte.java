package org.it.servlet.reporte;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.it.beans.Reportes;
import org.it.manejadores.ManejadorReportes;

public class ServletEditarReporte extends HttpServlet{

	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta) throws IOException,ServletException{
		doPost(peticion,respuesta);
	}
		
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta) throws IOException,ServletException{
		RequestDispatcher despachador=null;
		try{
			despachador=peticion.getRequestDispatcher("index.jsp");
			Reportes r=new Reportes();
			r.setDescripcion(peticion.getParameter("txtDescripcion"));
			r.setIdEstado(Integer.parseInt(peticion.getParameter("selectEstado")));
			r.setIdReporte(Integer.parseInt(peticion.getParameter("idReporte")));
			ManejadorReportes.INSTANCIA.editarReporte(r);			
		}catch(Exception e){
			e.printStackTrace();
			despachador=peticion.getRequestDispatcher("index.jsp");
		}
		despachador.forward(peticion,respuesta);
	}

}
