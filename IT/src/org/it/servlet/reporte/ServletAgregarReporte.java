package org.it.servlet.reporte;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.it.beans.Reportes;
import org.it.manejadores.ManejadorReportes;
import org.it.manejadores.ManejadorUsuario;
import java.util.Date;

public class ServletAgregarReporte extends HttpServlet{

	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta) throws IOException,ServletException{
		doPost(peticion,respuesta);
	}
		
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta) throws IOException,ServletException{
		RequestDispatcher despachador=peticion.getRequestDispatcher("index.jsp");
		try{
			Reportes rep=new Reportes();
			rep.setFechaReporte(new Date());
			rep.setDescripcion(peticion.getParameter("txtDescripcionReporte"));
			rep.setDPI(ManejadorUsuario.INSTANCIA.getUsuarioAutenticad().getDPI());
			rep.setIdArea(Integer.parseInt(peticion.getParameter("selectEstado")));
			rep.setIdEstado(1);
			ManejadorReportes.INSTANCIA.agregarReporte(rep);
		}catch(Exception e){
			e.printStackTrace();
		}
		despachador.forward(peticion,respuesta);
	}

}