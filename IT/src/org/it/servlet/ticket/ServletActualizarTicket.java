package org.it.servlet.ticket;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.it.beans.Ticket;
import org.it.manejadores.ManejadorTicket;
import java.util.Date;

public class ServletActualizarTicket extends HttpServlet{

	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta) throws IOException,ServletException{
		doPost(peticion,respuesta);
	}
		
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta) throws IOException,ServletException{
		RequestDispatcher despachador=peticion.getRequestDispatcher("index.jsp");
		try{
			Ticket t=new Ticket();
			t.setFin(new Date());
			t.setNotas(peticion.getParameter("txtNotas"));
			t.setIdEstado(Integer.parseInt(peticion.getParameter("selectEstado")));
			t.setIdTicket(Integer.parseInt(peticion.getParameter("idTicket")));
			ManejadorTicket.INSTANCIA.actualizarTicket(t);
		}catch(Exception e){
			e.printStackTrace();
		}
		despachador.forward(peticion,respuesta);
	}

}
