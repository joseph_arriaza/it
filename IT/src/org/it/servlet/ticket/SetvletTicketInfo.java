package org.it.servlet.ticket;

import java.io.IOException;

import org.it.beans.Ticket;
import org.it.manejadores.ManejadorTicket;
import org.it.manejadores.ManejadorUsuario_Ticket;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SetvletTicketInfo extends HttpServlet{

	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta) throws IOException,ServletException{
		doPost(peticion,respuesta);
	}
		
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta) throws IOException,ServletException{
		RequestDispatcher despachador=peticion.getRequestDispatcher("ticket.jsp");
		try{
			Ticket t=ManejadorTicket.INSTANCIA.getTicket(Integer.parseInt(peticion.getParameter("idTicket")));
			peticion.setAttribute("ticketInformacion",t);
			peticion.setAttribute("listaUsuarios",ManejadorUsuario_Ticket.INSTANCIA.getTicket(t.getIdTicket()));
		}catch(Exception e){
			e.printStackTrace();
			despachador=peticion.getRequestDispatcher("index.jsp");
		}
		despachador.forward(peticion,respuesta);
	}
	
}
