package org.it.servlet.ticket;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.it.manejadores.ManejadorTicket;
import org.it.beans.Ticket;

public class ServletEditarTicket extends HttpServlet{

	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta) throws IOException,ServletException{
		doPost(peticion,respuesta);
	}
		
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta) throws IOException,ServletException{
		RequestDispatcher despachador=peticion.getRequestDispatcher("index.jsp");
		try{
			Ticket t=new Ticket();
			t.setNombreTicket(peticion.getParameter("txtNombreTicket"));
			t.setDescripcion(peticion.getParameter("txtDescripcionTicket"));
			t.setIdTicket(Integer.parseInt(peticion.getParameter("idTicket")));
			ManejadorTicket.INSTANCIA.editarTicket(t);
		}catch(Exception e){
			e.printStackTrace();
		}
		despachador.forward(peticion,respuesta);
	}
}
