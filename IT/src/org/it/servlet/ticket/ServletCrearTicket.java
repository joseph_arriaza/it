package org.it.servlet.ticket;

import java.io.IOException;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.it.beans.Ticket;
import org.it.beans.Reportes;
import org.it.beans.Usuario_Ticket;
import org.it.manejadores.ManejadorTicket;
import org.it.manejadores.ManejadorReportes;
import org.it.manejadores.ManejadorUsuario_Ticket;

public class ServletCrearTicket extends HttpServlet{

	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta) throws IOException,ServletException{
		doPost(peticion,respuesta);
	}
		
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta) throws IOException,ServletException{
		RequestDispatcher despachador=peticion.getRequestDispatcher("index.jsp");
		try{
			Reportes rep=ManejadorReportes.INSTANCIA.getReporte(Integer.parseInt(peticion.getParameter("idReporte")));
			rep.setIdEstado(2);
			Ticket t=new Ticket();
			t.setNombreTicket(peticion.getParameter("txtNombreTicket"));
			t.setDescripcion(peticion.getParameter("txtDescripcionTicket"));
			t.setInicio(new Date());
			t.setIdEstado(1);
			t.setIdArea(Integer.parseInt(peticion.getParameter("txtIdArea")));
			ManejadorReportes.INSTANCIA.editarReporte(rep);
			ManejadorTicket.INSTANCIA.agregarTicket(t);
			int idTicket=ManejadorTicket.INSTANCIA.getLastId();
			int cantidad=Integer.parseInt(peticion.getParameter("txtCant"));
			if(cantidad>0){
				for(int i=0;i<cantidad;i++){
					if(peticion.getParameter("chk"+i)!=null){
						long dpi=Long.parseLong(peticion.getParameter("chk"+i));
						ManejadorUsuario_Ticket.INSTANCIA.agregarUsuario_Ticket(new Usuario_Ticket(dpi,idTicket));
					}
				}
			}else{
				despachador=peticion.getRequestDispatcher("abrir_ticket.jsp");
				peticion.setAttribute("estadoTicket","Debes agregar al menos un tecnico al ticket");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		despachador.forward(peticion,respuesta);
	}

}