<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="org.it.manejadores.ManejadorUsuario"  %>
<%@ page import="org.it.manejadores.ManejadorRol"  %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>IT</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

   <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.jsp">IT</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="ServletDesautenticar.do">Cerrar Sesión</a></li>
          </ul>
        </div>
      </div>
    </nav>
	<% if(ManejadorUsuario.INSTANCIA.getUsuarioAutenticad()!=null){   %>
	    <div class="container-fluid">
	      <div class="row">
	        <div class="col-sm-3 col-md-2 sidebar">
	          <ul class="nav nav-sidebar">
	          	<li><a href="index.jsp">Inicio</a></li>
	          	<li ><a href="#">Reportes<span class="sr-only">(current)</span></a></li>
	            <% if(ManejadorUsuario.INSTANCIA.getUsuarioAutenticad().getIdRol()==1){   %>
            		<li class="active"><a href="users.jsp">Usuarios</a></li>
            	<% }else{%>
            		<li class="active"><a href="users.jsp">Mi perfil</a></li>	
            	<% }%>
	            <li><a href="area.jsp">Areas</a></li>
	          </ul>
	        </div>
	    </div>
	    <% if(ManejadorUsuario.INSTANCIA.getUsuarioAutenticad().getIdRol()==1){   %>
	    	<div class="col-sm-offset-3 col-md-8 col-md-offset-2 main">
		        <h2 class="page-header">Usuarios</h2>
		        <div class="row placeholders">
		        	<div class="row">
		        		<div class="col-md-8">
		        			<table class="table table-hover">
					 			<thead>
					 				<tr>	
						 				<td>DPI</td>
						 				<td>Email</td>
							 			<td>Nombre</td>
							 			<td>Rol</td>
						 			</tr>
					 			</thead>
					 			<tbody>	
					 				<c:forEach var="usuario" items="<%= ManejadorUsuario.INSTANCIA.getLista()%>">
					 					<tr>
					 						<td><a href="ServletInfoUsuario.do?dpi=${usuario.getDPI()}">${usuario.getDPI()}</a></td>
							 				<td>${usuario.getEmail()}</td>
								 			<td>${usuario.getNombre()}</td>
								 			<td>${usuario.getRol()}</td>
					 					</tr>
					 				</c:forEach>
					 			</tbody>
					 		</table>
		        		</div>
		        		<div class="col-md-4">
		        			${estadoAdd}<br>
							<form method="POST" action="ServletAgregarUsuario.do">
							  <div class="form-group">
							    <label>DPI</label>
							    <input type="numeric" class="form-control" name="txtDPI" placeholder="DPI" required>
							  </div>
							  <div class="form-group">
							    <label>Nombre</label>
							    <input type="text" class="form-control" name="txtNombre" placeholder="Nombre" required>
							  </div>
							  <div class="form-group">
							    <label>Email</label>
							    <input type="email" name="txtEmail" class="form-control" placeholder="Correo electronico" required>
							  </div>
							  <div class="form-group">
							    <label>Contraseña</label>
							    <input type="password" class="form-control" name="txtPass" placeholder="Contraseña" required>
							  </div>
							  <div class="form-group">
							    <label>Rol</label>
							    <select class="form-control" name="selectRol">
								    	<c:forEach var="rol" items="<%= ManejadorRol.INSTANCIA.getLista()%>">
								    		<option value="${rol.getIdRol()}">${rol.getNombreRol()}</option>
								    	</c:forEach>
		      						  </select>
							  </div>
							  <button type="submit" class="btn btn-default">Agregar</button>
							</form>
						</div>	
		        	</div>
		        </div>
		    </div>
		<% } else {  %>
			<div class="col-sm-offset-3 col-md-8 col-md-offset-2 main">
		        <h2 class="page-header">Mi perfil</h2>
		        <div class="row placeholders">
		        	<div class="row">
		        		<div class="col-md-7">
						  <form method="POST" action="ServletEditarUsuario.do?idU=<%=ManejadorUsuario.INSTANCIA.getUsuarioAutenticad().getDPI()%>">
							  <div class="form-group">
							    <label>DPI</label>
							    <input type="text" disabled class="form-control" name="dpi" value="<%=ManejadorUsuario.INSTANCIA.getUsuarioAutenticad().getDPI()%>" required>
							  </div>
							  <div class="form-group">
							    <label>Nombre</label>
							    <input type="text" class="form-control" name="txtNombre" value="<%=ManejadorUsuario.INSTANCIA.getUsuarioAutenticad().getNombre()%>" required>
							  </div>
							  <div class="form-group">
							    <label>Email</label>
							    <input type="email" disabled name="txtEmail" class="form-control"  value="<%=ManejadorUsuario.INSTANCIA.getUsuarioAutenticad().getEmail()%>" required>
							  </div>
							  <div class="form-group">
							    <label>Contraseña</label>
							    <input type="password" class="form-control" name="txtPass" placeholder="Contraseña" required>
							  </div>
							  <button type="submit" class="btn btn-default">Editar</button>
						  </form>
						  <p><br>
						  ${estadoUsuario}<br><br>
					</div>	
		        	</div>
		        </div>
		    </div>
		<% }  %>
    <% }else{ %>
    	<div class="col-sm-offset-3 col-md-7 col-md-offset-2 main">
          <h1 class="page-header">Iniciar Sesión</h1>
          <div class="row placeholders">
            <form class="form-horizontal" method="POST" action="ServletAutenticar.do">
			  <div class="form-group">
			    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
			    <div class="col-sm-10">
			      <input type="email" name="txtEmail" class="form-control" id="inputEmail3" placeholder="Correo electrónico">
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputPassword3" class="col-sm-2 control-label">Contraseña</label>
			    <div class="col-sm-10">
			      <input type="password" name="txtPass" class="form-control" id="inputPassword3" placeholder="Contraseña">
			    </div>
			  </div>
			  </div>
			  <div class="form-group">
			    <div class="col-sm-offset-2 col-sm-10">
			      <button type="submit" class="btn btn-default">Iniciar</button>
			      <p>${estadoLogin}
			    </div>
			  </div>
			</form>
          </div>
         </div>
    <% }%>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="assets/js/vendor/holder.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>