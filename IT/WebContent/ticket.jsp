<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="org.it.manejadores.ManejadorUsuario"  %>
<%@ page import="org.it.manejadores.ManejadorEstado"  %>
<%@ page import="org.it.manejadores.ManejadorUsuario_Ticket"  %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>IT</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

   <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.jsp">IT</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="ServletDesautenticar.do">Cerrar Sesión</a></li>
          </ul>
        </div>
      </div>
    </nav>
	<% if(ManejadorUsuario.INSTANCIA.getUsuarioAutenticad()!=null){   %>
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="index.jsp">Inicio<span class="sr-only">(current)</span></a></li>
            <li><a href="#">Reports</a></li>
            <li><a href="users.jsp">Usuarios</a></li>
	        <li><a href="area.jsp">Areas</a></li>
          </ul>
        </div>
    </div>
    <% if(ManejadorUsuario.INSTANCIA.getUsuarioAutenticad().getIdRol()==1 || ManejadorUsuario.INSTANCIA.getUsuarioAutenticad().getIdRol()==3){   %>
 		<div class="col-sm-offset-3 col-md-8 col-md-offset-2 main">
	        <h2 class="page-header">Información del Ticket #${ticketInformacion.getIdTicket()}</h2>
	        <div class="row placeholders">
	        		<p><p><p>
			        <div class="row">
					  <div class="col-md-4">
					  	<p>
					  	<form method="POST" action="ServletEditarTicket.do?idTicket=${ticketInformacion.getIdTicket()}">
						  <div class="form-group">
						    <label for="exampleInputEmail1">Nombre del Ticket</label>
						    <input type="text" required name="txtNombreTicket" class="form-control" value="${ticketInformacion.getNombreTicket()}">
						  </div>
						  <div class="form-group">
						    <label for="exampleInputPassword1">Descripcion del Ticket</label>
						    <textarea required style="resize:none;" name="txtDescripcionTicket" class="form-control" rows="5">${ticketInformacion.getDescripcion()}</textarea>
						  </div>
						  <button type="submit" class="btn btn-default">Editar</button>
						</form>
					  	<label></label><br><br>
						<br><br>
					  </div>
					  <div class="col-md-4">
					  	<h4>Estado del Ticket</h4><p>
					  	<label>${ticketInformacion.getNombreE()}</label><br><br>
					  	<h4>Area del Ticket</h4><p>
					  	<label>${ticketInformacion.getNombreA()}</label><br><br>
					  	<h4>Fecha Inicio</h4><p>
					  	<label>${ticketInformacion.getInicio()}</label><br><br>
					  </div>
					  <div class="col-md-4">
					  	<h4>Responsables del Ticket</h4><p>
					  	<table class="table table-hover">
				 			<thead>
				 				<tr>
					 				<td>DPI</td>
					 				<td>Nombre</td>
					 			</tr>
				 			</thead>
		 					<tbody>
							  	<c:forEach var="usuario" items="${listaUsuarios}">
					        		<tr>
					        			<td>${usuario.getDPI()}</td>
					        			<td>${usuario.getNombre()}</td>
					        		</tr>
					        	</c:forEach>
				        	</tbody>
			        	</table>
					  </div>
					</div>
					 <div class="col-md-4">
					  	<script>
					  		if("${ticketInformacion.getFin()}"!="")
					  			document.write("<h4>Fecha Fin</h4><p><label>${ticketInformacion.getFin()}</label><br><br><br>");
					  		if("${ticketInformacion.getNotas()}"!="")
					  			document.write("<h4>Notas</h4><p><label>${ticketInformacion.getNotas()}</label><br><br><br>");
						</script>				  				  	
					  </div>
					  <div class="col-md-6">
					  	<h4>Modificar Ticket</h4><p>
					  	<form method="POST" action="ServletActualizarTicket.do?idTicket=${ticketInformacion.getIdTicket()}">
						  <div class="form-group">
						    <label for="exampleInputName2">Notas</label>
						    <textarea required style="resize:none;" name="txtNotas" class="form-control" rows="5">${ticketInformacion.getNotas()}</textarea>
						  </div>
						  <div class="form-group">
						    <label for="exampleInputEmail2">Estado</label>
						    <select class="form-control" name="selectEstado">
						    	<c:forEach var="estado" items="<%= ManejadorEstado.INSTANCIA.getLista()%>">
						    		<option value="${estado.getIdEstado()}">${estado.getEstado()}</option>
						    	</c:forEach>
      						  </select>
						  </div>
						  <button type="submit" class="btn btn-default">Modificar</button>
						</form>				  	
					  </div>			
	    	</div>
		</div>
	<% }   %>
    <% }else{ %>
    	<div class="col-sm-offset-3 col-md-7 col-md-offset-2 main">
          <h1 class="page-header">Iniciar Sesión</h1>
          <div class="row placeholders">
            <form class="form-horizontal" method="POST" action="ServletAutenticar.do">
			  <div class="form-group">
			    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
			    <div class="col-sm-10">
			      <input type="email" name="txtEmail" class="form-control" id="inputEmail3" placeholder="Correo electrónico">
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputPassword3" class="col-sm-2 control-label">Contraseña</label>
			    <div class="col-sm-10">
			      <input type="password" name="txtPass" class="form-control" id="inputPassword3" placeholder="Contraseña">
			    </div>
			  </div>
			  </div>
			  <div class="form-group">
			    <div class="col-sm-offset-2 col-sm-10">
			      <button type="submit" class="btn btn-default">Iniciar</button>
			      <p>${estadoLogin}
			    </div>
			  </div>
			</form>
          </div>
         </div>
    <% }%>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="assets/js/vendor/holder.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>