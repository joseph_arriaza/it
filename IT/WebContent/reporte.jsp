<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="org.it.manejadores.ManejadorUsuario"  %>
<%@ page import="org.it.manejadores.ManejadorEstado"  %>
<%@ page import="org.it.manejadores.ManejadorArea"  %>
<%@ page import="org.it.manejadores.ManejadorReportes"  %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>IT</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

   <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.jsp">IT</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="ServletDesautenticar.do">Cerrar Sesión</a></li>
          </ul>
        </div>
      </div>
    </nav>
	<% if(ManejadorUsuario.INSTANCIA.getUsuarioAutenticad()!=null){   %>
	    <div class="container-fluid">
	      <div class="row">
	        <div class="col-sm-3 col-md-2 sidebar">
	          <ul class="nav nav-sidebar">
	          	<li><a href="index.jsp">Inicio</a></li>
	          	<li  class="active"><a href="#">Reportes<span class="sr-only">(current)</span></a></li>
	            <% if(ManejadorUsuario.INSTANCIA.getUsuarioAutenticad().getIdRol()==1){   %>
            		<li><a href="users.jsp">Usuarios</a></li>
            	<% }else{%>
            		<li><a href="users.jsp">Mi perfil</a></li>	
            	<% }%>
	            <li><a href="area.jsp">Areas</a></li>
	          </ul>
	        </div>
	    </div>
	    <% if(ManejadorUsuario.INSTANCIA.getUsuarioAutenticad().getIdRol()==2){   %>
	 		<div class="col-sm-offset-3 col-md-8 col-md-offset-2 main">
	 			<% if(request.getAttribute("reporte")!=null){ %>
			        <h2 class="page-header">Información del Reporte #${reporte.getIdReporte()}</h2>
			        <div class="row placeholders">
			        		<p><p><p>
					        <div class="row">
							  <div class="col-md-4">
							  	<h4>Descripcion del Reporte</h4><p>
							  	<form method="POST" action="ServletEditarReporte.do?idReporte=${reporte.getIdReporte()}">
								  <div class="form-group">
								    <label for="exampleInputEmail1">Descripcion</label>
								    <textarea required style="resize:none;" name="txtDescripcion" class="form-control" rows="5">${reporte.getDescripcion()}</textarea>
								  </div>
								  <div class="form-group">
								    <label for="exampleInputPassword1">Estado</label>
								    <select class="form-control" name="selectEstado">
								    	<c:forEach var="estado" items="<%= ManejadorEstado.INSTANCIA.getLista()%>">
								    		<option value="${estado.getIdEstado()}">${estado.getEstado()}</option>
								    	</c:forEach>
		      						  </select>
								  </div>
								  <button type="submit" class="btn btn-default">Editar</button>
								</form>
							  </div>
							  <div class="col-md-4">
							  	<h4>Area del Reporte</h4><p>
							  	<label>${reporte.getArea()}</label><br><br>
							  	<h4>Estado del Reporte</h4><p>
							  	<label>${reporte.getEstado()}</label><br><br>
							  </div>
							  <div class="col-md-4">
							  	<h4>Reportador</h4><p>
							  	<table class="table table-hover">
						 			<thead>
						 				<tr>
							 				<td>DPI</td>
							 				<td>Nombre</td>
							 			</tr>
						 			</thead>
				 					<tbody>
									  	<tr>
							        		<td>${reportador.getDPI()}</td>
							        		<td>${reportador.getNombre()}</td>
							        	</tr>
						        	</tbody>
					        	</table>
					        	<h4>Fecha Reporte</h4><p>
							  	<label>${reporte.getFechaReporte()}</label><br><br>
							  </div>
							</div>
							 <div class="col-md-4">
							  	<script>
							  		if("${ticketInformacion.getFin()}"!=""){
							  			document.write("<h4>Fecha Fin</h4><p><label>${ticketInformacion.getFin()}</label><br><br><br>");
							  			document.write("<h4>Notas</h4><p><label>${ticketInformacion.getNotas()}</label><br><br><br>");
							  		}
								</script>				  				  	
							  </div>			
			    	</div>
			   <% }else{%>
			   		<h2 class="page-header">Agregar Reporte</h2>
			        <div class="row placeholders">
			        	<p><p><p>
					    <div class="row">
					    	 <div class="col-md-6">
								<form method="POST" action="ServletAgregarReporte.do">
								  <div class="form-group">
								    <label for="exampleInputEmail1">Descripcion</label>
								    <textarea placeholder="Descripcion" required style="resize:none;" name="txtDescripcionReporte" class="form-control" rows="5"></textarea>
								  </div>
								  <div class="form-group">
								    <label for="exampleInputPassword1">Area</label>
								    <select class="form-control" name="selectEstado">
								    	<c:forEach var="area" items="<%= ManejadorArea.INSTANCIA.getLista()%>">
								    		<option value="${area.getIdArea()}">${area.getNombreArea()}</option>
								    	</c:forEach>
		      						  </select>
								  </div>
								  <button type="submit" class="btn btn-default">Agregar</button>
								</form> 
							 </div>
						</div>		
			    	</div>
			   <% }%>
			</div>
		<% }else{   %>
			<div class="col-sm-offset-3 col-md-10 col-md-offset-2 main">
		        <h2 class="page-header">Reportes</h2>
		        <div class="row placeholders">
		        <table class="table">
		        	<thead>
		        		<tr>
			        		<td>#</td>
			        		<td>Descripcion</td>
			 				<td>Fecha</td>
			 				<td>Reportador</td>
			 				<td>Area</td>
		 					<td>Estado</td>
		 					<td>Acciones</td>
	 					</tr>
		        	</thead>
		        	<tbody>
			        	<c:forEach var="reporte" items="<%= ManejadorReportes.INSTANCIA.getListaPorRevisar()%>">
							<tr class="${reporte.getClase()}">
				        		<td>${reporte.getIdReporte()}</td>
				        		<td>${reporte.getDescripcion()}</td>
				        		<td>${reporte.getFechaReporte()}</td>
				        		<td>${reporte.getDPI()}</td>
				        		<td>${reporte.getArea()}</td>	
				        		<td>${reporte.getEstado()}</td>
				        		<td><a href="SetvletAbrirTicket.do?idReporte=${reporte.getIdReporte()}">Abrir Ticket</a></td>
				        	</tr>
						</c:forEach>
					</tbody>	
				</table>					  		
		    	</div>
			</div>
		<% }   %>
    <% }else{ %>
    	<div class="col-sm-offset-3 col-md-7 col-md-offset-2 main">
          <h1 class="page-header">Iniciar Sesión</h1>
          <div class="row placeholders">
            <form class="form-horizontal" method="POST" action="ServletAutenticar.do">
			  <div class="form-group">
			    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
			    <div class="col-sm-10">
			      <input type="email" name="txtEmail" class="form-control" id="inputEmail3" placeholder="Correo electrónico">
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputPassword3" class="col-sm-2 control-label">Contraseña</label>
			    <div class="col-sm-10">
			      <input type="password" name="txtPass" class="form-control" id="inputPassword3" placeholder="Contraseña">
			    </div>
			  </div>
			  </div>
			  <div class="form-group">
			    <div class="col-sm-offset-2 col-sm-10">
			      <button type="submit" class="btn btn-default">Iniciar</button>
			      <p>${estadoLogin}
			    </div>
			  </div>
			</form>
          </div>
         </div>
    <% }%>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="assets/js/vendor/holder.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>