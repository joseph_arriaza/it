<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="org.it.manejadores.ManejadorUsuario"  %>
<%@ page import="org.it.manejadores.ManejadorEstado"  %>
<%@ page import="org.it.manejadores.ManejadorReportes"  %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>IT</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

   <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.jsp">IT</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="ServletDesautenticar.do">Cerrar Sesión</a></li>
          </ul>
        </div>
      </div>
    </nav>
	<% if(ManejadorUsuario.INSTANCIA.getUsuarioAutenticad()!=null){   %>
	    <div class="container-fluid">
	      <div class="row">
	        <div class="col-sm-3 col-md-2 sidebar">
	          <ul class="nav nav-sidebar">
	          	<li><a href="index.jsp">Inicio</a></li>
	          	<li class="active"><a href="reporte.jsp">Reportes</a></li>
	            <% if(ManejadorUsuario.INSTANCIA.getUsuarioAutenticad().getIdRol()==1){   %>
            		<li><a href="users.jsp">Usuarios</a></li>
            	<% }else{%>
            		<li><a href="users.jsp">Mi perfil</a></li>	
            	<% }%>
	            <li><a href="area.jsp">Areas</a></li>
	          </ul>
	        </div>
	    </div>
	    <% if(ManejadorUsuario.INSTANCIA.getUsuarioAutenticad().getIdRol()==1||ManejadorUsuario.INSTANCIA.getUsuarioAutenticad().getIdRol()==3){   %>
	 		<div class="col-sm-offset-3 col-md-8 col-md-offset-2 main">
		        <h2 class="page-header">Abrir Ticket para el Reporte #${reporte.getIdReporte()}</h2>
		        <div class="row placeholders">
		        		<p><p><p>
				        <div class="row">
						  <div class="col-md-4">
						  	<h4>Descripcion del Reporte</h4><p>
							  <div class="form-group">
							    <label for="exampleInputEmail1">Descripcion</label>
							    <textarea  disabled style="resize:none;" name="txtDescripcion" class="form-control" rows="5">${reporte.getDescripcion()}</textarea>
							  </div>
							  <div class="form-group">
							    <label for="exampleInputPassword1">Estado</label>
							    <input type="text" disabled class="form-control" name="txtEstado" value="${reporte.getEstado()}">
							  </div>
						  </div>
						  <div class="col-md-4">
						  	<br><br>
						   	  <div class="form-group">
							    <label for="exampleInputPassword1">Area del Reporte</label>
							    <input type="text" disabled class="form-control" name="txtEstado" value="${reporte.getArea()}">
							  </div>
							  <div class="form-group">
							    <label for="exampleInputPassword1">Fecha Reporte</label>
							    <input type="text" disabled class="form-control" name="txtEstado" value="${reporte.getFechaReporte()}">
							  </div>
						  </div>
						  <div class="col-md-4">
						  <br><br>
						  	<h4>Reportador</h4><p>
						  	<table class="table table-hover">
					 			<thead>
					 				<td>DPI</td>
					 				<td>Nombre</td>
					 			</thead>
			 					<tbdoy>
								  	<tr>
						        		<td>${reportador.getDPI()}</td>
						        		<td>${reportador.getNombre()}</td>
						        	</tr>
					        	</tbdoy>
				        	</table>
				        	<br><br>
				        	${estadoTicket}
						  </div>
						</div>
						 <div class="col-md-6">
						 	<br><br>
						 	<h4>Agregar usuarios para el Ticket</h4><p>
						 	<table class="table">
					        	<thead>
					        		<tr>
						        		<td>Agregar</td>
						        		<td>DPI</td>
						 				<td>Nombre</td>
				 					</tr>
					        	</thead>
					        	<tbody>
					        		<% int name=0;%>
						        	<c:forEach var="usuario" items="<%= ManejadorUsuario.INSTANCIA.getListaTecnicos()%>">
										<tr>
											<form method="POST" action="ServletCrearTicket.do?idReporte=${reporte.getIdReporte()}">
							        		<td><input type="checkbox" name="chk<%=name%>" value="${usuario.getDPI()}"></td>
							        		<td>${usuario.getDPI()}</td>
							        		<td>${usuario.getNombre()}</td>
							        	</tr>
							        	<% name+=1;%>
									</c:forEach>
								</tbody>	
							</table>				 	
						 </div>	
						<div class="col-md-4">
							<br><br>
							<div class="form-group">
								<label for="exampleInputEmail1">Nombre del Ticket</label>
								<input type="text" required class="form-control" name="txtNombreTicket" placeholder="Nombre del Ticket" maxlength="30">
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Descripcion</label>
								<textarea required style="resize:none;" name="txtDescripcionTicket" class="form-control" rows="5">${reporte.getDescripcion()}</textarea>
							</div>
						</div>		
						<div class="col-md-2">
							<br><br><br>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<input type="hidden" name="txtCant" value="<%=name%>">
									<input type="hidden" name="txtIdArea" value="${reporte.getIdArea()}">
							    	<button type="submit" class="btn btn-default">Abrir ticket</button>
							    </div>
							</div>
							</form>
						</div>		
		    	</div>
			</div>
		<% }else{   %>
			
		<% }   %>
    <% }else{ %>
    	<div class="col-sm-offset-3 col-md-7 col-md-offset-2 main">
          <h1 class="page-header">Iniciar Sesión</h1>
          <div class="row placeholders">
            <form class="form-horizontal" method="POST" action="ServletAutenticar.do">
			  <div class="form-group">
			    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
			    <div class="col-sm-10">
			      <input type="email" name="txtEmail" class="form-control" id="inputEmail3" placeholder="Correo electrónico">
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputPassword3" class="col-sm-2 control-label">Contraseña</label>
			    <div class="col-sm-10">
			      <input type="password" name="txtPass" class="form-control" id="inputPassword3" placeholder="Contraseña">
			    </div>
			  </div>
			  </div>
			  <div class="form-group">
			    <div class="col-sm-offset-2 col-sm-10">
			      <button type="submit" class="btn btn-default">Iniciar</button>
			      <p>${estadoLogin}
			    </div>
			  </div>
			</form>
          </div>
         </div>
    <% }%>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="assets/js/vendor/holder.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>